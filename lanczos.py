from numpy import pi, sin, cos, sinc
import scipy.integrate as integrate
from itertools import islice


class FourierSeries:

    def __init__(self, f, n, period=2*pi):
        """
        Construct `FourierSeries` object representing Fourier series approximation of function `f`
        periodical on `(x_min, x_max)` interval with regard to at most `n`-th degree coefficients.

        :param f: one variable function being approximated
        :param n: degree of highest degree coefficient in series representation
        :param period: length of the interval being period of approximated function
        """
        self.period, self.omega = period, 2 * pi / period
        self.a_coef = [self.a(f, k) for k in range(n)]
        self.b_coef = [self.b(f, k) for k in range(n)]

    def a(self, f, n):
        """Return the n-th cosine coefficient of Fourier series representation of function f."""
        return integrate.quad(lambda x: f(x) * cos(n * self.omega * x), 0, self.period)[0] / self.period

    def b(self, f, n):
        """Return the n-th sine coefficient of Fourier series representation of function f."""
        return integrate.quad(lambda x: f(x) * sin(n * self.omega * x), 0, self.period)[0] / self.period

    def evaluate(self, x):
        """Return value of approximated function at point x."""
        val = self.a_coef[0]
        for k, (a_k, b_k) in islice(enumerate(zip(self.a_coef, self.b_coef)), 1, None):
            val += 2. * (a_k * cos(k * self.omega * x) + b_k * sin(k * self.omega * x))
        return val

    def lanczos_coef(self, k):
        """Return the Lanczos sigma coefficient standing by k-th sine/cosine coefficient."""
        return sinc(float(k) / (len(self.a_coef) - 1))

    def lanczos(self, x):
        """Return value of approximated function at point x using Lanczos sigma approximation method."""
        val = self.a_coef[0]
        for k, (a_k, b_k) in islice(enumerate(zip(self.a_coef, self.b_coef)), 1, None):
            val += 2. * self.lanczos_coef(k) * (a_k * cos(k * self.omega * x) + b_k * sin(k * self.omega * x))
        return val
